# Mon Interface de Commande Personnalisée
Ce projet implémente une interface de commande personnalisée en Bash qui permet aux utilisateurs de réaliser diverses tâches telles que la gestion de fichiers, le changement de répertoire, et l'envoi d'e-mails.


## Explication des sections
- **Fonctionnalités** : Décrit ce que le script peut faire.
- **Prérequis** : Ce dont l'utilisateur a besoin pour exécuter le script.
- **Installation** : Comment configurer le script pour qu'il soit prêt à l'emploi.
- **Utilisation** : Comment démarrer et utiliser le script.
- **Sécurité** : Avertissements et conseils pour la gestion sécurisée des mots de passe.
- **Licence** : Informe les utilisateurs de leurs droits légaux par rapport au script.

## Fonctionnalités
- **Gestion de fichiers** : Liste, suppression, et ouverture de fichiers.
- **Gestion de répertoires** : Changement de répertoire et suppression.
- **Informations système** : Affichage de l'heure courante et des informations sur le programme.
- **Utilisateur** : Connexion, affichage et modification de profil utilisateur.
- **Téléchargement** : Téléchargement de pages web.
- **E-mails** : Fonctionnalité mock pour envoyer des e-mails.

## Prérequis
Le script nécessite un environnement Unix/Linux avec les outils suivants installés :
- Bash
- Curl (pour le téléchargement de pages web)
- Mailutils (pour l'envoi d'e-mails, optionnel)

## Installation
1. **Cloner le dépôt** :
```bash
git clone https://gitlab.com/xraiizen/my-magic-prompt-morvan-etienne.git
cd my-magic-prompt-morvan-etienne
chmod +x main.sh functions.sh quit.sh
```
## Utilisation
Pour démarrer l'interface, exécutez le script main.sh depuis le terminal :
```bash
./main.sh
 ```
Une fois démarré, vous serez invité à vous connecter.
### Identifiant par default
nom d'utilisateur: `user`     
mot de passe: `pass`

Après la connexion, vous pouvez utiliser les commandes disponibles affichées par la commande help.

Commandes disponibles:      
`help` : Affiche toutes les commandes disponibles.        
`ls` : Liste tous les fichiers et répertoires.        
`rm <fichier>` : Supprime un fichier spécifié.        
`rmdir <répertoire>` : Supprime un répertoire spécifié.       
`about` : Affiche des informations sur le programme.  
`version` : Affiche la version du programme.      
`pwd` : Affiche le répertoire courant.        
`hour` : Affiche l'heure actuelle.        
`httpget <url> <file>` : Télécharge une page web à l'URL spécifiée.       
`smtp` : Envoie un e-mail (fonctionnalité simulée).       
`open` <file> : Ouvre un fichier dans l'éditeur VIM.      

## Sécurité
Les mots de passe sont stockés dans un fichier password.txt, qui doit être sécurisé. Ne stockez pas de mots de passe en texte clair dans des environnements de production.

## Licence
Ce projet est distribué sous la licence MIT. Voir le fichier [LICENCE](https://gitlab.com/xraiizen/my-magic-prompt-morvan-etienne/-/blob/main/LICENSE?ref_type=heads) pour plus d'informations.
