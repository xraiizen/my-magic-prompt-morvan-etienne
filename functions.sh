# Affiche la liste des commandes disponibles pour l'utilisateur.
show_help() {
    echo "Available commands:"
    echo "- help, ?: Display this help message"
    echo "- ls: List all files and directories, including hidden ones"
    echo "- rm <file>: Remove a file"
    echo "- rmdir <directory>: Remove a directory"
    echo "- rmdirwtf <directory...>: Remove one or more directories with password protection"
    echo "- about: Show information about this program"
    echo "- version, --v, vers: Show version of this program"
    echo "- age: Enter your age and I'll tell if you're adult or not"
    echo "- quit: Exit the prompt"
    echo "- profil: Display user profile"
    echo "- passw: Change password (Mock functionality)"
    echo "- cd: Change directory"
    echo "- pwd: Show current directory"
    echo "- hour: Show current hour"
    echo "- httpget <url> <file>: Download webpage HTML"
    echo "- smtp: Send an email (Mock functionality)"
    echo "- open: Open a file in VIM"
    echo "- rps: Play Rock Paper Scissors game for two players"
}
# Liste tous les fichiers et répertoires, y compris les cachés.
list_files() {
    ls -la
}
# Supprime un fichier spécifié.
remove_file() {
    if [ -z "$1" ]; then
        echo "Please specify a file to remove."
    else
        rm "$1"
        echo "$1 removed."
    fi
}
# Supprime un répertoire spécifié.
remove_directory() {
    if [ -z "$1" ]; then
        echo "Please specify a directory to remove."
        return
    fi
    rm -r "$1"
    if [ $? -eq 0 ]; then
        echo "$1 removed."
    else
        echo "Failed to remove $1."
    fi
}
# Affiche des informations sur ce programme, y compris la version.
about() {
    echo "My Magic Prompt Version 1.0"
    echo "A custom command-line interface for various tasks."
}
# Affiche la version du programme.
show_version() {
     echo -e "${GREEN}Version 1.0${NC}"
}
# Demande à l'utilisateur son âge et affiche si c'est un adulte ou non.
check_age() {
    read -p "Please enter your age: " age
    if [ "$age" -lt 18 ]; then
        echo -e "${RED}You are a minor.${NC}"
    else
        echo -e "${GREEN}You are an adult.${NC}"
    fi
}
# Affiche le profil de l'utilisateur.
show_profile() {
    local stored_password=$(cat password.txt)
    echo "Username: $USERNAME"
    echo "Password: $stored_password"
}
# Permet de changer son mot de passe.
change_password() {
    read -s -p "Enter new password: " new_password
    echo
    echo $new_password > password.txt
    echo "Password changed."
}
# Change le répertoire courant à celui
change_directory() {
     if [ -z "$1" ]; then
        echo "Please specify a directory."
    else
        cd $1
    fi
}
# Affiche le répertoire courant.
show_current_directory() {
    pwd
}
# Affiche l'heure courante.
show_hour() {
    date '+%H:%M:%S'
}
# Télécharge une page web à l'URL spécifiée et la sauvegarde dans un fichier.
download_webpage() {
    read -p "Enter the URL: " url
    if [ -z "$url" ]; then
        echo -e "${RED}Please specify a URL.${NC}"
        return
    fi
    read -p "Enter the filename to save the HTML to: " filename
    if [ -z "$filename" ]; then
        echo -e "${RED}Please specify a filename.${NC}"
        return
    fi
    curl -o "$filename" "$url"
    if [ $? -eq 0 ]; then
        echo -e "${GREEN}Webpage downloaded successfully and saved to $filename.${NC}"
    else
        echo -e "${RED}Failed to download the webpage.${NC}"
    fi
}
# Envoie un email au destinataire spécifié. Demande l'installation de mailutils si non disponible.
send_email() {
    # Vérifier si la commande `mail` est disponible
    if ! command -v mail &> /dev/null; then
        echo -e "${RED}The 'mail' command could not be found.${NC}"
        read -p "Do you want to install mailutils? (yes/no): " response
        if [[ "$response" == "yes" ]]; then
            if [[ -f /etc/debian_version ]]; then
                echo "Installing mailutils on Debian/Ubuntu..."
                sudo apt-get update && sudo apt-get install mailutils
            elif [[ -f /etc/redhat-release ]]; then
                echo "Installing mailx on Red Hat/Fedora..."
                sudo dnf install mailx
            else
                echo -e "${RED}Package manager not detected. Please install mailutils or mailx manually.${NC}"
                return
            fi
        
            if ! command -v mail &> /dev/null; then
                echo -e "${RED}Failed to install 'mail'. Please try to install mailutils or mailx manually.${NC}"
                return
            fi
        else
            return
        fi
    fi

    read -p "Enter recipient's email address: " recipient
    read -p "Enter subject: " subject
    echo "Enter your message (Ctrl-D to end):"
    msg=$(cat)
    
    echo "$msg" | mail -s "$subject" "$recipient"
    if [ $? -eq 0 ]; then
        echo -e "${GREEN}Email sent successfully to $recipient.${NC}"
    else
        echo -e "${RED}Failed to send email.${NC}"
    fi
}
# Ouvre un fichier spécifié dans l'éditeur VIM.
open_file() {
    if [ -z "$1" ]; then
        echo "Please specify a file to open."
    else
        vim "$1"
    fi
}
# Fonction pour jouer à pierre-papier-ciseaux avec deux joueurs.
play_rps() {
    echo "Enter Player 1 name:"
    read p1_name
    echo "Enter Player 2 name:"
    read p2_name

    p1_score=0
    p2_score=0
    rounds=3

    for ((i = 1; i <= rounds; i++)); do
        echo "$p1_name's turn (rock, paper, scissors):"
        read p1_choice
        echo "$p2_name's turn (rock, paper, scissors):"
        read p2_choice

        # Implementing SuperKitty power
        if [[ $p1_choice == "SuperKitty" ]]; then
            echo "$p1_name wins with SuperKitty!"
            ((p1_score+=3))
            break
        elif [[ $p2_choice == "SuperKitty" ]]; then
            echo "$p2_name wins with SuperKitty!"
            ((p2_score+=3))
            break
        fi

        # Regular game logic
        case $p1_choice-$p2_choice in
            rock-scissors | scissors-paper | paper-rock)
                echo "$p1_name wins this round!"
                ((p1_score++))
                ;;
            scissors-rock | paper-scissors | rock-paper)
                echo "$p2_name wins this round!"
                ((p2_score++))
                ;;
            *)
                echo "It's a tie this round."
                ;;
        esac
    done

    if (( p1_score > p2_score )); then
        echo "$p1_name wins the game with $p1_score points!"
    elif (( p2_score > p1_score )); then
        echo "$p2_name wins the game with $p2_score points!"
    else
        echo "The game is a tie."
    fi
}

# Fontion pour supprimer un ou plussieurs répertoires avec une confirmation de mot de passe.
rmdirwtf() {
    echo "This operation requires a password."
    read -sp "Enter password: " input_password
    stored_password=$(cat password.txt)

    if [[ $input_password != $stored_password ]]; then
        echo -e "${RED}Incorrect password. Operation cancelled.${NC}"
        return
    fi

    echo "Enter directory or directories to remove:"
    read directories
    rm -r $directories
    echo "Directories removed."
}

# Fonction de connexion qui vérifie les identifiants de l'utilisateur.
login() {
    local stored_password=$(cat password.txt)
    echo "Please login to continue:"
    read -p "Username: " input_username
    read -s -p "Password: " input_password
    echo
    if [[ $input_username == $USERNAME && $input_password == $stored_password ]]; then
        echo -e "${GREEN}Login successful.${NC}"
    else
        echo -e "${RED}Login failed.${NC}"
        quit
    fi
}