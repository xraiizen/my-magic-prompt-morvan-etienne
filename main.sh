#!/bin/bash
# Charge les scripts nécessaires pour que toutes les fonctions et variables soient disponibles.
source quit.sh
source constants.sh
source functions.sh

# Fonction cmd qui détermine quelle commande a été entrée par l'utilisateur et appelle la fonction correspondante.
cmd() {
  cmd=$1
  argv=$*

# Structure de contrôle qui appelle différentes fonctions basées sur l'entrée de l'utilisateur.
  case "$cmd" in
    help|?) show_help ;; 
    ls) list_files ;;
    rm*) remove_file "${@:2}" ;;
    rmdir*) remove_directory "${@:$2}" ;;
    about) about ;;
    age) check_age ;;
    version|--v|vers) show_version ;;
    cd) change_directory "${@:2}" ;;
    pwd) show_current_directory ;;
    hour) show_hour ;;
    httpget*) download_webpage "${@:2}" "${@:3}" ;;
    smtp) send_email ;;
    open*) open_file "${@:2}" ;;
    profil) show_profile ;;
    passw) change_password ;;
    rps) play_rps ;;
    rmdirwtf) rmdirwtf ;;
    quit|exit) quit;;
    *) echo "Unknown command: $cmd" ;;
  esac
}

# Fonction principale qui lance le script.
main() {
  login
  lineCount=1

  while true; do
    local date=$(date +%H:%M)
    echo -ne "$date - [\033[31m$lineCount\033[m] - \033[33mXzen\033[m ~ ☠️ ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}
# Appel de la fonction main pour démarrer le script.
main
