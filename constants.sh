#!/bin/bash
# Définition des couleurs pour l'affichage dans le terminal
export RED='\033[0;31m'
export GREEN='\033[0;32m'
export BLUE='\033[0;34m'
export NC='\033[0m' # No Color

# Identifiant utilisateur - pour la démonstration.
export USERNAME="user"
